dnl  GMP specific autoconf macros


dnl  Copyright 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2009, 2011 Free
dnl  Software Foundation, Inc.
dnl
dnl  This file is part of the GNU MP Library.
dnl
dnl  The GNU MP Library is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU Lesser General Public License as published
dnl  by the Free Software Foundation; either version 3 of the License, or (at
dnl  your option) any later version.
dnl
dnl  The GNU MP Library is distributed in the hope that it will be useful, but
dnl  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
dnl  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
dnl  License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public License
dnl  along with the GNU MP Library.  If not, see http://www.gnu.org/licenses/.


dnl  Some tests use, or must delete, the default compiler output.  The
dnl  possible filenames are based on what autoconf looks for, namely
dnl
dnl    a.out - normal unix style
dnl    b.out - i960 systems, including gcc there
dnl    a.exe - djgpp
dnl    a_out.exe - OpenVMS DEC C called via GNV wrapper (gnv.sourceforge.net)
dnl    conftest.exe - various DOS compilers


define(IA64_PATTERN,
[[ia64*-*-* | itanium-*-* | itanium2-*-*]])

dnl  Need to be careful not to match m6811, m6812, m68hc11 and m68hc12, all
dnl  of which config.sub accepts.  (Though none of which are likely to work
dnl  with GMP.)
dnl
define(M68K_PATTERN,
[[m68k-*-* | m68[0-9][0-9][0-9]-*-*]])

define(POWERPC64_PATTERN,
[[powerpc64-*-* | powerpc64le-*-* | powerpc620-*-* | powerpc630-*-* | powerpc970-*-* | power[3-9]-*-*]])

define(S390_PATTERN,
[[s390-*-* | z900esa-*-* | z990esa-*-* | z9esa-*-* | z10esa-*-* | z196esa-*-*]])

define(S390X_PATTERN,
[[s390x-*-* | z900-*-* | z990-*-* | z9-*-* | z10-*-* | z196-*-*]])

define(X86_PATTERN,
[[i?86*-*-* | k[5-8]*-*-* | pentium*-*-* | athlon-*-* | viac3*-*-* | geode*-*-* | atom-*-*]])

define(X86_64_PATTERN,
[[athlon64-*-* | k8-*-* | k10-*-* | bobcat-*-* | bulldozer-*-* | pentium4-*-* | atom-*-* | core2-*-* | corei*-*-* | x86_64-*-* | nano-*-*]])

