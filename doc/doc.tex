\documentclass[a4paper]{article}

\usepackage{geometry}
\usepackage[T1]{fontenc}
\usepackage{aeguill}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xspace}
\usepackage{url}
\usepackage{tikz}
\usetikzlibrary{patterns}
\usetikzlibrary{decorations.pathreplacing}
\usepackage{float}
\usepackage{xcolor}
\usepackage{enumerate}
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}

% [Charles] things I'm using all the time
\newcommand{\ie}{\textit{i.e.},\xspace}
\newcommand{\eg}{\textit{e.g.},\xspace}
\newcommand{\esp}[1]{\mathbb{E} \bigl[ #1 \bigr] }
\newcommand{\prob}[1]{\mathbb{P} \bigl[ #1 \bigr] }
\newcommand{\Esp}[1]{\mathbb{E} \biggl[ #1 \biggr] }
\newcommand{\Prob}[1]{\mathbb{P} \biggl[ #1 \biggr] }
\newcommand{\card}[1]{ \bigl| #1 \bigr| }
\newcommand{\bige}{\mathbf{E}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\bigO}[1]{\ensuremath{\mathcal{O}\left( #1 \right)} }
\newcommand{\bigOmega}[1]{\ensuremath{\Omega\left( #1 \right)} }
\newcommand{\bigTheta}[1]{\ensuremath{\Theta\left( #1 \right)} }
\newcommand{\concat}{\, || \,}
\newcommand{\bits}{\left\{0,1\right\}}
\renewcommand{\epsilon}{\varepsilon}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
%\newcommand{\blue}[1]{#1}

\newcommand{\replace}[2]{\sout{#1} {#2}}
\newcommand{\V}{\ensuremath{\left(\mathbb{F}_2\right)^n}\xspace}
\newcommand{\D}[2]{\frac{\partial#1}{\partial#2}}
\newcommand{\Ftwo}{\ensuremath{\mathbb{F}_2}\xspace}
\newcommand{\FtwoN}{\ensuremath{\left(\mathbb{F}_2\right)^n}\xspace}
\newcommand{\FtwoU}{\ensuremath{\left(\mathbb{F}_2\right)^u}\xspace}

\newcommand{\x}{\vec{\mathbf{x}}}
\newcommand{\y}{\vec{\mathbf{y}}}
\newcommand{\z}{\vec{\mathbf{z}}}
\newcommand{\tr}[1]{{}^t#1}

%Taiwanese things
\newcommand{\GF}[1]{\ensuremath{\mathbb{F}_{#1}}\xspace}
\newcommand{\MQ}{\ensuremath{\mathcal MQ}\xspace}
\newcommand{\bb}{\ensuremath{\mathbf b}}
\newcommand{\cc}{\ensuremath{{\mathbf c}}\xspace}
\newcommand{\ww}{\ensuremath{\mathbf w}}
\newcommand{\xx}{\ensuremath{{\mathbf x}}\xspace}
\newcommand{\yy}{\ensuremath{{\mathbf y}}\xspace}
\newcommand{\zz}{\ensuremath{{\mathbf z}}\xspace}
\newcommand{\rr}{\ensuremath{\rightarrow}}
\newcommand{\mt}{\ensuremath{\mapsto}}
\newcommand{\xor}{\texttt{ xor  }}
\newcommand{\ffour}{\ensuremath{\mathbf{F_4}}\xspace}
\newcommand{\ffive}{\ensuremath{\mathbf{F_5}}\xspace}
\newcommand{\xmm}{\ensuremath{\mathtt{xmm}}\xspace}
\newcommand{\comment}{\ding{110}\ding{43}\textcolor{red}}

\newif\iflong
\longtrue

\begin{document}

%\floatstyle{boxed} \restylefloat{figure}
\floatstyle{plain} \restylefloat{figure}

\title{notes on implementing fast exhaustive search}
\author{Charles Bouillaguet}

\maketitle


\algrenewcommand{\algorithmiccomment}[1]{$//$ #1}

Algo.~\ref{algo:base} shows most generic code for quadratic equations
($d=2$).

\begin{algorithm}
\caption{Starting Point}
\label{algo:base}
\begin{algorithmic}[1]
\Procedure{Zeroes}{$f$}
\State $State \leftarrow \textsc{Init}^{[d]}(f,0,0)$
\State \textbf{if} $\y = 0$ \textbf{then} report $f(0) = 0$

\For{$i$ \textbf{from} 1 \textbf{to} $2^{n}-1$}
\State \textbf{let} $k_1 = b_1(i)$ \textbf{in}
\State \textbf{let} $k_2 = b_2(i)$ \textbf{in}

\State \textbf{if} $k_2 \neq \bot $ \textbf{then} 
$D\left[k_1\right] \leftarrow D\left[k_1\right] \oplus D\left[k_1,k_2\right]$
\State $\y \leftarrow \y \oplus D\left[k_1\right]$
\State \textbf{if} $\y = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(i)\right) = 0$

\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

Here, $b_i(i)$ returns the index of the $i$-th bit of the integer $i$
set to one. For instance, $b_1(1) = 0$, $b_1(8) = 3$ and $b_1(10) =
1$. When the hamming weight of $i$ is less than $k$, then $b_k(i)$ is
not defined, and returns a special value $\bot$.

% \section{Initialisation}

% Réglons une bonne fois pour toute la question de l'initialisation,
% c'est-à-dire l'état des choses laissé en place par $State \leftarrow
% \textsc{Init}^{[d]}(f,0,0)$. Pour cela, on va utiliser les notations
% suivantes. La fonction qu'on souhaite énumérer est $F : \FtwoN
% \rightarrow \FtwoU$, et elle s'écrit:
% \[
% F(x) = \begin{pmatrix}
%   F_1(x) \\
%   \vdots \\
%   F_u(x)
% \end{pmatrix}
% = Q(x) + L \cdot x + c
% \]
% où $L$ est une matrice de taille $u \times n$, $c\in \FtwoU$, et $Q$
% est un vecteur de $u$ formes quadratiques en $n$ variables. On notera
% $Q_i$ la $i$-ème de ces formes quadratiques, et on admettra que $Q_i$
% est représenté par une matrice $M_i$ (de sorte que $Q_i(x) = \tr{x}
% \cdot M_i \cdot x$).


% \subsubsection{Constantes.} Les constantes sont essentiellement les
% dérivées secondes des polynômes d'entrée (donc $n^2$ vecteurs de $u$
% bits). $D[i,j]$ est donc un vecteur de $u$ bits, qui s'écrit (si j'ai
% bien compris)~:
% \[
% D[i,j] = \Big( (M_1 + \tr{M_1})[i,j], \dots, (M_u + \tr{M_u})[i,j] \Big)
% \]

% \subsubsection{État interne.} Très clairement, la valeur initiale de
% $\y$ est $f(0)$, c'est-à-dire la composante constante de $f$. On a de
% plus~:
% \[
% D[i] = \begin{cases}
%   L \cdot e_0 & \text{si } i=0\\
%   L \cdot e_i + D[i][i-1] & \text{sinon }
% \end{cases}
% \]
% Dans le fond, ceci suggère de faire des vecteurs $L\cdot e_i$ des
% ``constantes''.

To be somewhat efficient, this loop must be unrolled, and the
conditional instructions must be removed as much as possible. This is
not very difficult.

\paragraph{Removing the ``IF''.} The annoying case $k_2 = \bot$ only
happens when $i$ is a power of two. Pour avoid it, we split the main
loop into $n$ ``macro-steps''. This results in
Algo.~\ref{algo:no_if}.

\begin{algorithm}
\caption{Without testing $k_2$}
\label{algo:no_if}

\begin{algorithmic}[1]
\Procedure{Step}{$x$}
\State \textbf{let} $k_1 = b_1(x)$ \textbf{in}
\State \textbf{let} $k_2 = b_2(x)$ \textbf{in}

\State $D\left[k_1\right] \gets D\left[k_1\right] \oplus D\left[k_1,k_2\right]$
\State $\y \gets \y \oplus D\left[k_1\right]$
\State \textbf{if} $\y = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(x)\right) = 0$
\EndProcedure

\vspace{0.5cm}

\Procedure{Steps}{$x,y$}
\For{$i$ from $x$ to $y-1$}
\State $\textsc{Step}(i)$
\EndFor 
\EndProcedure

\vspace{0.5cm}

\Procedure{Zeroes}{$f$}
\State $State \leftarrow \textsc{Init}^{[d]}(f,0,0)$
\State \textbf{if} $\y = 0$ \textbf{then} report $f(0) = 0$

\For{$j$ \textbf{from} 0 \textbf{to} $n-1$} \Comment{$j$-th macro-step}

\State $\y \leftarrow \y \oplus D\left[j\right]$
\State \textbf{if} $\y = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(2^j)\right) = 0$

\State \Comment{Now we know that $b_2(...) \neq \bot$}

\State $\textsc{Steps}\left(2^j+1, 2^{j+1}\right)$
\EndFor

\EndProcedure
\end{algorithmic}
\end{algorithm}

\paragraph{Unrolling.} Now, we must unroll the loop. This means that
we will write down a piece of code that implements a fixed number of
\textsc{Step}s, say $2^L$, and that we will wrap the required
machinery around it. The best value of $L$ should be determined
experimentally (larger values reduced the overhead of the loop, but
may result in a piece of code that does not fit in the cache. Values
around 8,9 or 10 seem reasonable). In the sequel, I will call a block
of $2^L$ \textsc{Step}s a \textsc{Chunk}. If we look back
to Algo.~\ref{algo:base}, then the $i$ index will be enumerated as
follows~:

\begin{tikzpicture}[scale=0.9]
  \useasboundingbox (0,-1.25) rectangle (15,2.25);

  \draw (0,0.5)  -- node[left] {$i$} (0,0) -- (15,0) -- (15,0.5);
  \foreach \x in {0,0.25,...,15}
    \draw (\x,0) -- (\x, -0.1);
  
  \filldraw[pattern=north west lines] (0,0) rectangle (3,0.5);
  
  \draw[->] (0.125,-0.7) node[anchor=north]{0} -- +(0, 0.65);
  \draw[->] (3.125,-0.7) node[anchor=north]{$L$} -- +(0, 0.65);
  \draw[->] (14.875,-0.7) node[anchor=north]{$n-1$} -- +(0, 0.65);


  \draw[decorate,decoration=brace] (0.1,0.7) -- node[above=0.2cm,text
  width=2.5cm,align=center] {varies inside a chunk} (2.9,0.7);

  \draw[decorate,decoration=brace] (3.1,0.7) -- node[above=0.2cm,text
  width=5cm,align=center] {varies in the outer loop}(14.9,0.7);
\end{tikzpicture}

Note that the first $L$ macro-steps [should not/cannot] be unrolled,
because there do not form a complete \textsc{Chunk}. It makes sense to
put a lot of effort into optimizing a single chunk, as for large
values of $n$, most of the running time will be spent inside
chunks. The $k$-th chunk of the $j$-th macro-step looks like this:
\begin{algorithmic}
\Procedure{CHUNK}{$j,k$}
\State $\textsc{Step}\left(2^j+ k \times 2^L \right)$
\State $\textsc{Step}\left(2^j+ k \times 2^L + 1 \right)$
\State $\textsc{Step}\left(2^j+ k \times 2^L + 2 \right)$
\State $\vdots$
\State $\textsc{Step}\left( 2^j+ k \times 2^L + 2^L - 1 \right)$
\EndProcedure
\end{algorithmic}

In most steps inside a chunk, the values of $k_1$ and $k_2$ can be
known in advance. More specifically, inside the $\ell$-th step, $k_1$
depends only on $\ell$ is $\ell \neq 0$, and $k_2$ depends only on
$\ell$ if $\ell$ is not a power of two. 

\begin{algorithm}
\caption{Split in chunks of size $2^L$}
\label{algo:chunks}

\begin{algorithmic}[1]

\Procedure{CHUNK}{$j,k$}
\State $\textsc{Steps}\left(2^j+ k \times 2^L, 2^j+ (k+1) \times 2^L\right)$
\EndProcedure

\vspace{0.5cm}

\Procedure{Zeroes}{$f$}
\State $State \gets \textsc{Init}^{[d]}(f,0,0)$
\State \textbf{if} $\y = 0$ \textbf{then} report $f(0) = 0$

\State \Comment{Deal with the small macro-steps}

\For{$j$ \textbf{from} 0 \textbf{to} $\min(n, L)$}

\State $\y \leftarrow \y \oplus D\left[j\right]$
\State \textbf{if} $\y = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(2^j)\right) = 0$

\State $\textsc{Steps}\left(2^j+1, 2^{j+1}\right)$

\EndFor

\State \Comment{Now split the large macro-steps in chunks}

\For{$j$ \textbf{from} $L+1$ \textbf{to} $n-1$}

\State $\y \leftarrow \y \oplus D\left[j\right]$
\State \textbf{if} $\y = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(2^j)\right) = 0$

\State $\textsc{Steps}\left(2^j+1, 2^{j}+2^L\right)$

%\For{$i$ \textbf{from} $1$ \textbf{to} $2^{L}- 1$}
%\State STEP$(2^j + i)$
%\EndFor

\For{$k$ \textbf{from} $1$ \textbf{to} $2^{j-L-1}$}
\State $\textsc{Chunk}(j, k)$

\EndFor
\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

Algorithm~\ref{algo:chunks} is the basis of my plain-C implementation.

\section{Parallelism}

We will try to address the situation where several SIMD units are
available. This situation is typical of multi-core CPUs (modern
Opterons have 16 cores, each with its own 4-way 32-bit SIMD unit), but
also corresponds to GPUs (A GTX 295 has 30 SIMD units, and each one is
16-way).

We must then write ``SIMD chunks'', i.e. chunks that execute $2^{L+S}$
steps using a $2^S$-way SIMD unit. There are several possible ways to
do this. My own guess is that we may have interest run on the same
SIMD unit the steps whose number share as many lowest-significant bits
as possible~:

\begin{tikzpicture}[scale=0.9]
  \path[use as bounding box] (0,-1.5) rectangle (15,2.25);

  \draw (0,0.5)  -- node[left] {$i$} (0,0) -- (15,0) -- (15,0.5);
  \foreach \x in {0,0.25,...,15}
    \draw (\x,0) -- (\x, -0.1);
  
  \filldraw[pattern=north west lines] (0,0) rectangle (3,0.5);
  \filldraw[pattern=crosshatch dots] (13,0) rectangle (15,0.5);
  \filldraw[pattern=north east lines] (10,0) rectangle (13,0.5);

  
  \draw[->] (0.125,-0.7) node[anchor=north]{0} -- +(0, 0.65);
  \draw[->] (3.125,-0.7) node[anchor=north]{$L$} -- +(0, 0.65);
  \draw[->] (14.875,-0.7) node[anchor=north]{$n-1$} -- +(0, 0.65);
  \draw[->] (13.125,-0.7) node[anchor=north]{$n-S$} -- +(0, 0.65);
  \draw[->] (10.125,-0.7) node[anchor=north]{$n-S-T$} -- +(0, 0.65);


  \draw[decorate,decoration=brace] (0.1,0.7) -- node[above=0.2cm,text width=2.5cm,align=center] {intra-chunk counter} (2.9,0.7);
  \draw[decorate,decoration=brace] (3.1,0.7) -- node[above=0.2cm,text width=5cm,align=center] {outer loop }(9.9,0.7);
  \draw[decorate,decoration=brace] (10.1,0.7) -- node[above=0.2cm,text width=2.5cm,align=center] {core counter} (12.9,0.7);
  \draw[decorate,decoration=brace] (13.1,0.7) -- node[above=0.2cm,text width=2.5cm,align=center] {SIMD thread counter} (14.9,0.7);
\end{tikzpicture}

The interest of doing so is that the pair $(b_1(i), b_2(i))$ has a
greater chance of depending only on the lowest-significant bits of
$i$, which means that the $2^S$ threads of a single SIMD unit have a
greater chance of accessing to the same constant $D[k_1,k_2]$. It is
then sufficient to fetch it \emph{once} in memory to feed the $2^S$
threads. This seems particularly suited to NVIDIA GPU's who are
capable of coalesced memory accesses.

If several SIMD units are available, it is very likely preferable to
parallelize the outer loop, i.e. the enumeration of the \emph{middle
  bits} of $i$ (paradoxically)...


\paragraph{Initialisation.} Implementing this idea will require a
slightly more sophisticated initialization procedure, as we must be
able to generate the internal state (the $\y$ and $D[\cdot]$ values)
that are ready to be used even if the loop starts ``in the middle''
(i.e. with $i > 1$). This is however not difficult. The second-order
derivatives (the $D[\cdot, \cdot]$) can be shared by all threads,
since they only depend on the equations. With this, we can write
algo~\ref{algo:simd_base}.

% Un problème à résoudre cependant, c'est qu'il va falloir initialiser
% un état interne pour chaque ``thread''. En fait, cela nécessite d'être
% capable de produire les valeurs de $y, D[0], \dots, D[n-1]$ pour une
% valeur (quasiment) arbitraire de $i$. Pour $y$, c'est très
% facile. Pour les $D[k]$, c'est un peu moins clair. On peut commencer
% par remarquer que la valeur de $D[k]$ n'est pas utilisée tant que
% $i<2^k$. Lorsque $i=2^k$, la valeur initiale de $D[k]$ est utilisée
% (on pourrait même dans le fond la définir à ce moment-là). Mais $D[k]$
% n'est pas modifié tant que $i < 2^k + 2^{k+1}$. Le problème de
% calculer $D[k]$ n'a donc un intérêt que si $i \geq 2^k + 2^{k+1}$, et
% on va donc se placer dans ce cas-là. En fait, en regardant
% l'algorithme~\ref{algo:base}, il n'est pas excessivement difficile
% d'écrire une expression générale de la valeur de $D[k]$ au début de la
% $i$-ème itération (ce qu'on va noter $D_i[k]$)~:
% \begin{align*}
% D_i[k] &= D_0[k] + \sum_{\substack{0\leq j < i\\ b_1(j) = k}} D[k,b_2(j)] \\
% %
%  &= D_0[k] + \sum_{0\leq 2^k + j 2^{k+1} < i} D[k,b_2(2^k + j2^{k+1})] \\
% %
%  &= D_0[k] + \sum_{j=1}^{(i-2^k)/2^{k+1}} D[k, k + 1 + b_1(j)] \\
% %
%  &= D_0[k] + D[k, \cdot] \times \sum_{j=1}^{\left\lfloor(i-2^k)/2^{k+1}\right\rfloor} e_{k + 1 + b_1(j)} \\
% %
%  &= D_0[k] + D[k, \cdot] \times \Big( \textsc{Gray}\left( \left\lfloor(i-2^k)/2^{k+1}\right\rfloor \right) \lll (k + 1) \Big) \\
% \end{align*}

% \subsection{Pseudo-code simple}


\begin{algorithm}
\caption{Very basic SIMD version}
\label{algo:simd_base}
\begin{algorithmic}[1]
\Procedure{Zeroes}{$f$}
\State $D[\cdot,\cdot] \gets \textsc{Init\_Constants}(f)$

\For{$t$ \textbf{from} 0 \textbf{to} $2^{S}-1$}
\State $\left(y_t,D_t[\cdot]\right) \gets \textsc{Init\_State}\left(f, t \cdot 2^{n-S}\right)$
\State \textbf{if} $\y_t = 0$ \textbf{then} report $f\left( t \cdot 2^{n-S} \right) = 0$
\EndFor


\For{$i$ \textbf{from} 1 \textbf{to} $2^{n-S}-1$}
\For{$t$ \textbf{from} 0 \textbf{to} $2^{S}-1$}

\State \textbf{let} $j_t = i +  t \cdot 2^{n-S}$ \textbf{in}

\State \textbf{let} $u_t = b_1(j_t)$ \textbf{in}
\State \textbf{let} $v_t = b_2(j_t)$ \textbf{in}

\State \textbf{if} $v_t \neq \bot$ \textbf{then} 
$D_t\left[u_t\right] \leftarrow D_t\left[u_t\right] \oplus D\left[u_t,v_t\right]$
\State $\y_t \leftarrow \y_t \oplus D_t\left[u_t\right]$
\State \textbf{if} $\y_t = 0$ \textbf{then} report $f\left(\textsc{GrayCode}(j_t)\right) = 0$
\EndFor

\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

\paragraph{Unrolling and avoiding tests again.} Unrolling the loop
with SIMD instructions is a teeny tiny bit harder than before, because
we ought to keep threads doing the same thing as much as possible. The
good thing is that $u_t$ only depend on $i$ (since $i>1$, it follows
that $u_t = b_1(i)$. The bad thing is that when $i$ is a power of two,
then $v_t$ depends on both $i$ and $t$. This means that inside a
single SIMD unit, some threads will fetch different constants
$D[\cdot, \cdot]$ than some others. Also, the zero-th thread still
has $v_t = \bot$ when $i$ is a power of two.

Long story short: we need to do something special when $i$ is a power
of two (in fact this boil down to precompute $n$ vectors of special
constants to use in place of the normal ones when $i$ is a power of
two).

With unrolling, we obtain Algo.~\ref{algo:simd_unrolled}, which is the
prototype of my SIMD implementation.


\begin{algorithm}
\caption{Improved SIMD version}
\label{algo:simd_unrolled}
\begin{algorithmic}[1]
\Procedure{ParallelStep}{$x$}
\State \textbf{let} $u = b_1(x)$ \textbf{in}
\State \textbf{let} $v = b_2(x)$ \textbf{in}
\For{$t$ \textbf{from} 0 \textbf{to} $2^{S}-1$}
\State $D_t\left[u\right] \leftarrow D_t\left[u\right] \oplus D\left[u,v\right]$
\State $\y_t \leftarrow \y_t \oplus D_t\left[u\right]$
\State \textbf{if} $\y_t = 0$ \textbf{then} report $f\left(\textsc{GrayCode}( x +  t \cdot 2^{n-S} \right) = 0$
\EndFor
\EndProcedure

\vspace{0.5cm}

\Procedure{SpecialParallelStep}{$x$}
\State \textbf{let} $u = b_1(x)$ \textbf{in}
\For{$t$ \textbf{from} 0 \textbf{to} $2^{S}-1$}
\State $D_t\left[u\right] \leftarrow D_t\left[u\right] \oplus SpecialD\left[u\right]$
\State $\y_t \leftarrow \y_t \oplus D_t\left[u\right]$
\State \textbf{if} $\y_t = 0$ \textbf{then} report $f\left(\textsc{GrayCode}( x + t \cdot 2^{n-S} \right) = 0$
\EndFor
\EndProcedure

\vspace{0.5cm}

\Procedure{Zeroes}{$f$}
\State $D[\cdot,\cdot], SpecialD[\cdot] \gets \textsc{Init\_Constants}(f)$

\For{$t$ \textbf{from} 0 \textbf{to} $2^{S}-1$}
\State $\left(y_t,D_t[\cdot]\right) \gets \textsc{Init\_State}\left(f, t \cdot 2^{n-S}\right)$
\State \textbf{if} $\y_t = 0$ \textbf{then} report $f\left( t \cdot 2^{n-S} \right) = 0$
\EndFor

\State \Comment{Deal with the small macro-steps}

\For{$j$ \textbf{from} 0 \textbf{to} $\min(n-S, L)$}
\State \textsc{Special ParallelStep}$\left(2^j \right)$
\For{$i$ \textbf{from} $2^j + 1$ \textbf{to} $2^{j+1}-1$}
\State \textsc{ParallelStep}($i$)
\EndFor
\EndFor

\State \Comment{Now split the large macro-steps in chunks}

\For{$j$ \textbf{from} $L+1$ \textbf{to} $n-S-1$}
\State \textsc{SpecialParallelStep}$\left(2^j \right)$
\For{$i$ \textbf{from} $2^j + 1$ \textbf{to} $2^{j+1}-1$}
\State \textsc{ParallelStep}($i$)
\EndFor
\EndFor


\EndProcedure
\end{algorithmic}
\end{algorithm}


% \section{Feintes !}

% pcmpeqw laisse le registre desitination à zéro, \textsf{sauf si une
%   solution est trouvée}. On peut donc, en mode optimiste, éviter de le
% remettre à zéro, mais le gros bête de gcc ne le sait pas.

% En plus, gcc tourne sur 5 registres, alors on pourrait en utiliser au moins 3 pour stocker d'autres trucs.

% Quand $b_2(...)$ n'est pas défini, alors les trucs sont accédés dans l'ordre $\rightarrow$ utiliser un compteur !

%Trucs variés pour calculer b_1 et b_2 + vite...

\end{document}

%\bibliographystyle{abbrv}
%\bibliography{charles}



