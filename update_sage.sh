#!/bin/sh
make distclean
cd ..
rm -rf fes-0.1/src/*
cp -r fes/* fes-0.1/src/
sage -pkg fes-0.1
sage -f fes-0.1.spkg
sage -b
sage -t /usr/local/sage/devel/sage/sage/libs/fes.pyx
