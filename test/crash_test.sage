# this tries to check that fes + the wrapper work correctly...
long_test = True

from sage.libs.fes import exhaustive_search

def gen_random_system(n,m,d):
    R = BooleanPolynomialRing(n, 'x')
    return Sequence([ sum([R.random_element(degree=degree) for degree in range(1,d+1)]) for i in range(m)  ])


def homogenize_system(F):
    I = ideal(F).homogenize()
    H = I.gens()
    R = I.ring()
    R_bool = BooleanPolynomialRing( R.ngens(), R.variable_names() )
    return Sequence([ R_bool(f) for f in I.homogenize().gens() ])

def test_sols(F, sols):
    for solution in sols:
        assert vector([ f.subs(solution) for f in F]) == 0

def test_random_system(n,m,d):
    R = BooleanPolynomialRing(n, 'x')
    solution = dict(zip(R.gens(), VectorSpace(GF(2), n).random_element()))
    F = gen_random_system(n,m,d)
    G = [ f + f.subs(solution) for f in F] # force known solution
    sols = exhaustive_search(G)
    assert solution in sols
    assert len(sols) == 1


def test_homogenization(F):
    sols_F = exhaustive_search(F)
    H = homogenize_system(F)
    sols_H = exhaustive_search(H)
    R = H.ring()
    h = R.gen(R.ngens()-1)
    for x in sols_F:
        x[h] = 1
        assert x in sols_H
    for x in sols_F:
        if x[h] == 1:
            del  x[h]
            assert x in sols_F


##################################################"

print "nearly trivial example"
R.<x,y,z> = BooleanPolynomialRing()
sols = exhaustive_search( [x*y + z + y + 1, x+y+z, x*y + y*z] )
assert sols == [{y: 0, z: 1, x: 1}]

print "Another trivial example"
R.<x,y,z,t,w> = BooleanPolynomialRing()
f = x*y*z + z + y + 1
sols = exhaustive_search( [f, x+y+z, x*y + y*z] )

print "Random Degree-2 System"
test_random_system(16, 32, 2)

print "Random Degree-2 System with lots of equations"
test_random_system(16, 96, 2)

print "Random Degree-2 System with ***lots*** of equations"
test_random_system(16, 1024, 2)


print "Random Degree-3 System"
test_random_system(14, 32, 3)

print "Homogeneous Degree-2 System"
test_homogenization(gen_random_system(16, 16, 2))

print "Homogeneous Degree-3 System"
test_homogenization(gen_random_system(16, 16, 3))


if long_test:
    print "Random Degree-4 System"
    test_random_system(16, 32, 4)

    print "Random Degree-5 System"
    test_random_system(16, 32, 5)

    print "Random Degree-6 System"
    test_random_system(16, 32, 6)

    print "Random Degree-6 System with lots of equations"
    test_random_system(16, 96, 6)

    print "Random Degree-9 System with ***lots*** of equations"
    test_random_system(16, 1024, 9)
