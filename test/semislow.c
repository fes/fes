#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include <assert.h>
#include <getopt.h>
#include <err.h>

#include "fes.h"
#include "idx_LUT.h"
#include "rand.h"

int terms_to_kill[10][32] = { {}, 
			      {},
			      {3, 5, 6, 8, 9, 10, 12, 13, 14, 15, 17, 18, 19, 20, 21, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 38, 39, 40, 41},
			      {7, 11, 13, 14, 18, 20, 21, 23, 24, 25, 29, 31, 32, 34, 35, 36, 38, 39, 40, 41, 45, 47, 48, 50, 51, 52, 54, 55, 56, 57, 59, 60},
			      {15, 23, 27, 29, 30, 38, 42, 44, 45, 49, 51, 52, 54, 55, 56, 64, 68, 70, 71, 75, 77, 78, 80, 81, 82, 86, 88, 89, 91, 92, 93, 95},
			      {31, 47, 55, 59, 61, 62, 78, 86, 90, 92, 93, 101, 105, 107, 108, 112, 114, 115, 117, 118, 119, 135, 143, 147, 149, 150, 158, 162, 164, 165, 169, 171},
			      {63, 95, 111, 119, 123, 125, 126, 158, 174, 182, 186, 188, 189, 205, 213, 217, 219, 220, 228, 232, 234, 235, 239, 241, 242, 244, 245, 246, 278, 294, 302, 306},
			      {127, 191, 223, 239, 247, 251, 253, 254, 318, 350, 366, 374, 378, 380, 381, 413, 429, 437, 441, 443, 444, 460, 468, 472, 474, 475, 483, 487, 489, 490, 494, 496},
			      {255, 383, 447, 479, 495, 503, 507, 509, 510, 638, 702, 734, 750, 758, 762, 764, 765, 829, 861, 877, 885, 889, 891, 892, 924, 940, 948, 952, 954, 955, 971, 979},
			      {511, 767, 895, 959, 991, 1007, 1015, 1019, 1021, 1022, 1278, 1406, 1470, 1502, 1518, 1526, 1530, 1532, 1533, 1661, 1725, 1757, 1773, 1781, 1785, 1787, 1788, 1852, 1884, 1900, 1908, 1912}};


void usage(char **argv) {
  printf("USAGE: ./%s [options]\n", argv[0]);
  printf("--verbose          print extra statistics\n");
  printf("--n k              run test with k variables\n");
  printf("--e k              run test with k equations\n");
  printf("--degree d         run test with degree-d equations\n");
  printf("--seed xxx         initialize the pseudo-random generator with the integer xxx\n");
  printf("--eliminate k      generate equations where the first k monomial (the the invlex order) are absent\n");

  exit(1);
}

int main(int argc, char** argv) {

  int n = 19;
  int n_eqs = 16;
  int degree = 2;
  int verbose = 0;
  unsigned long random_seed = 1;
  int el = 0;
  int ch;


  struct option longopts[7] = {
    { "verbose",     no_argument,          NULL,            'v' },
    { "n",           required_argument,    NULL,            'n' },
    { "e",           required_argument,    NULL,            'e' },
    { "degree",      required_argument,    NULL,            'd' },
    { "seed",        required_argument,    NULL,            's' },
    { "eliminate",   required_argument,    NULL,            'k' },
    { NULL,         0,                     NULL,             0 }
  };

  while ((ch = getopt_long(argc, argv, "", longopts, NULL)) != -1)
    switch (ch) {
    case 'v': verbose = 1; break;
    case 'n': n = atoi(optarg); break;
    case 'e': n_eqs = atoi(optarg); break;
    case 'd': degree = atoi(optarg); break;
    case 's': random_seed = atoi(optarg); break;
    case 'k': el = atoi(optarg); break;
    default: usage(argv);
    }

  if (n > 64 || n_eqs > 32) {
    printf("This hack can only handle up to 64 vars and 32 equations\n");
    exit(1);
  }

  // initialize lookup tables
  idx_lut_t* idx_table = init_deginvlex_LUT(n, degree);
  LUT_t LUT = idx_table->LUT;

  mysrand(random_seed);

  // initalize a random system
  const int N = n_monomials(n,degree);
  pck_vector_t *F = calloc(N, sizeof(pck_vector_t));
  if (F == NULL) {
    perror("impossible to allocate memory for the coefficients");
    exit(1);
  }
  for(int i=0; i<N; i++) {
    F[i] = myrand() & ((1ll << n_eqs) - 1);
  }

  for(int i=0; i<el; i++)
    F[ terms_to_kill[degree][i] ] = 0;

  if (verbose) printf("this is the (very slow) reference implementation...\n");

  // now, exhaustive search
  // this indirectly test the "tester_deg_x" autogenerated functions...
  for(uint64_t i=0; i<(1ull << n); i++)
    if (packed_eval(LUT, n, degree, F, i) == 0)
      printf("%08" PRIx64 "\n", i);

  fflush(stdout);
  free_LUT(idx_table);
  return 0;
}
